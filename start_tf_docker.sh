#!/bin/bash         

docker run --gpus all -it -p 6006:6006 \
	--name=tensorflow \
	--mount type=bind,source="$(pwd)",target=/home/tensorflow/training \
	--rm romandmo/tfrd02 bash
