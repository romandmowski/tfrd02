# Tensorflow 2 Object Detection example - Docker image

Inside this folder you can find all necessery components to run the Transfer Learning example based on this article: https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html 

List of items:

- file **start_tf_docker.sh** - sh script to run this example
- folder **model01** - Python scripts used in the Transfer Learning example
- folder **model01/images** - labeled images splited to 'test' and 'train' sets
- folder **model01/annotations** - TensorFlow *.record files, which contain the list of annotations for the dataset images 
- folder **model01/pre-trained-models** - the downloaded pre-trained model (ssd_resnet50_v1_fpn_640x640_coco17_tpu-8), which shall be used as a starting checkpoint for training jobs
- folder **model01/models** - this folder will contain a sub-folder for each of training job. Each subfolder will contain the training pipeline configuration file *.config, as well as all files generated during the training and evaluation of our model.


Docker image allows us to prepare a container consists all necessary components: Ubuntu, Nvidia CUDA and cuDNN libraries, Tensorflow2 (TF2) and Tensorflow Object Detection API.

The script **start_tf_docker.sh** starts the docker container:

`docker run --gpus all -it -p 6006:6006 \
	--name=tensorflow --mount type=bind,source="$(pwd)",target=/home/tensorflow/training --rm romandmo/tfrd02 bash`

Additionaly the current folder is mounted inside Docker container as a   **/home/tensorflow/training folder**


Inside the docker container go inside the folder with model_main_tf2.py file and run command:

`python3 model_main_tf2.py --model_dir=models/my_ssd_resnet50 --pipeline_config_path=models/my_ssd_resnet50/pipeline.config`



